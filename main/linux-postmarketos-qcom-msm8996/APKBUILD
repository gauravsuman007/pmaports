# Maintainer: Yassine Oudjana (Tooniis) <y.oudjana@protonmail.com>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="postmarketos-qcom-msm8996"
pkgname=linux-$_flavor
pkgver=5.14.0
pkgrel=2
pkgdesc="Kernel close to mainline with extra patches for Qualcomm MSM8996 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/msm8996-mainline/linux-msm8996"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-nftables"
makedepends="bison findutils flex installkernel openssl-dev perl"

# Source
_tag=v${pkgver//_/-}-msm8996
source="
	linux-msm8996-$_tag.tar.gz::$url/-/archive/$_tag/linux-msm8996-$_tag.tar.gz
	config-$_flavor.$arch
"
builddir="$srcdir/linux-msm8996-$_tag"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION=$((pkgrel + 1 ))
}

package() {
	mkdir -p "$pkgdir"/boot
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
}
sha512sums="
620075d5e9bcbd424461bebdc73eaac79b8a26a1e15f8cb931b5b9e1181829f7f11341567a63007ced6757da1f2a10a9d85fb5e8f9a91f76d7ebc115e410ed46  linux-msm8996-v5.14.0-msm8996.tar.gz
ad82b1a16e8ec71e1340b7d7fdfcb223ef387bfeb709ab67fa5dc7f6b9c70a9948c15b88b37dd557643307efb7facdddb74aa8addf2cfe53ebf2241dd53b4513  config-postmarketos-qcom-msm8996.aarch64
"
